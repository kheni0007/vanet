%average filter

function Xfilt=avgfilter(X,size)
Xfilt=X;

if nargin<2
   size=3;
elseif mod(size,2)==0
   size=size+1;
end

hsize=round(size/2); %halfsize

for ii=(1+hsize):1:(length(X)-hsize)
   Xfilt(ii)=mean(X([(ii-hsize):(ii+hsize)]));
end
end
