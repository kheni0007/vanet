%DSR Routing Protocol

close all
clear
clc
warning off

%PARAMETERS
%==========

%Field Dimensions - x and y maximum (in meters)
xm=850;
ym=850;

%Number of Nodes in the field
N=30


prompt={'Enter the number of nodes'};
%N =inputdlg(prompt);               %Cut off frequency input
%N=str2double(N);

%maximum transmission distance
dmax=250; 

vmax=1500;  %maximum velocity of nodes

% maximum number of rounds
rmax=250;

% packet request probability
Preq = 0.1;

%Creation of the random Mobile Network
%======================================
marker = imread('markers/logo1.png');
marker = imresize(marker, 0.05);

figure(1)
xlim([0 xm]);
ylim([0 ym]);
hold on;
for i=1:1:N
    S(i).xd=rand(1,1)*xm;
    S(i).yd=rand(1,1)*ym;
    S(i).G=0;
	S(i).xv=(rand*-1 +rand)*vmax*(1/sqrt(2));
	S(i).yv=(rand*-1 +rand)*vmax*(1/sqrt(2));
    angle = find_angle(S(i).xv, S(i).yv);
    marker2 = imrotate(marker, angle);
    marker2(marker2==0) = 255;
    % https://stackoverflow.com/questions/38761718/custom-markers-for-matlab-plot
    imagesc([(S(i).xd -1/2) (S(i).xd -1/2)], [(S(i).yd -1/2) (S(i).yd -1/2)], marker2)
	S(i).DN=i;
	S(i).Q=[];
	S(i).RRQ=i;
    S(i).Data='';
end
hold off


%START OF ROUNDS
%================
sumPacket = 0;
for r=1:1:rmax


  %counter for bit transmitted 
  packets_SENT=0; % successful packet
  packets=0;   %total packet
  delay = 0;   % network delay
  
  %counter for packets sent per round
  PACKETS(r)=0;
  PACKETS_SENT(r)=0;
  
  % counter for delay
  DELAY(r)=0;

  %move the nodes in a random manner
  S=move(S,xm,ym,vmax);

  %Form requests
  S=request(S, Preq);
  
  %find the paths to send requests
  S=findpaths(S,dmax);  
  
  % plot the movement of cars and packets in a figure
  h = figure(2);
  xlim([0 xm]);
  ylim([0 ym]);
  clf(h)
  hold on
  for i=1:1:N
     angle = find_angle(S(i).xv, S(i).yv);
     marker2 = imrotate(marker, angle);
     marker2(marker2==0) = 255;
     imagesc([(S(i).xd -1/2) (S(i).xd -1/2)], [(S(i).yd -1/2) (S(i).yd -1/2)], marker2) 
     S(i).G=0;
     % https://www.mathworks.com/matlabcentral/answers/160487-how-can-i-draw-a-line-with-arrow-head-between-2-data-points-in-a-plot
     quiver(S(i).xd ,S(i).yd, S(S(i).RRQ).xd - S(i).xd, S(S(i).RRQ).yd - S(i).yd,'r'); 
  end
  hold off
%   pause(0.01);
  
    for i=1:1:N  

        %Sending route requests

        if (S(i).RRQ~=i)  % routuing link for non-last nodes
            %broadcast message
            %the recipient adds to the queue
            S(S(i).RRQ).Q=[S(S(i).RRQ).Q S(i).DN];
            S(S(i).RRQ).Data = strcat(num2str(S(S(i).DN).xd),'/', num2str(S(S(i).DN).yd),'/', num2str(S(S(i).DN).xv),'/', num2str(S(S(i).DN).yv),'/',num2str(r));
            % route request  headers
            S(i).RRQ=i;	
            packets = packets+1;
            packets_SENT = packets_SENT + 1;

            if (S(i).DN==S(i).RRQ)|| (rand <Preq) % link for last node
                S(i).DN=i;
                if isempty(S(i).Q)~=1
                    S(i).RRQ = S(i).Q(1);
                else
                    S(i).RRQ = i;
                end
                cell = split(S(1).Data,'/');
                r_start = str2double(cell{end});
                delay = delay + (r - r_start);
                packets = packets+2;
                packets_SENT = packets_SENT + 1;
            end
        end     


        % perfrom queuing operations for next round
        if isempty(S(i).Q)~=1
            if S(i).Q(1)==S(i).DN
                S(i).Q(1)=[];
            else
                S(i).DN = S(i).Q(1);
                S(i).Q(1) = [];
            end
        else
            S(i).RRQ = i;
            S(i).DN = i;
        end
    end
    PACKETS_SENT(r)=packets_SENT;
    PACKETS(r) = packets;
    DELAY(r) = delay;
    sumPacket = [sumPacket sumPacket(end)+packets];
end



%Display of Results
%==================

close all

clc

figure(1)
plot(PACKETS_SENT);
xlabel('Number of rounds');
ylabel('Packets sent');

figure(2)
PDR=(PACKETS_SENT./PACKETS)*100;
PDR(isnan(PDR))=100;
PDR=avgfilter(PDR,10);
plot(PDR);
xlabel('Rounds');
ylabel('Packets Delivered');
ylim([0 100]);
title('Packet Delivery Ratio');

figure(3)
LPR=((PACKETS - PACKETS_SENT)./PACKETS)*100;
LPR(isnan(LPR))=0;
LPR=avgfilter(LPR,10);
plot(LPR);
xlabel('Rounds');
ylabel('Loss Packet Ratio');
ylim([0 100]);
title('Loss Packet Ratio');

figure(4)
DELAY(isnan(DELAY))=0;
DELAY=avgfilter(DELAY,10);
plot((DELAY./PACKETS_SENT)*30)
xlabel('Rounds');
ylabel('Avg End to End Delay (ms)');
title('End to End Delay');
