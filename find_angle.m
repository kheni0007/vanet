function angle = find_angle(xv, yv)
%FIND_ANGLE Function to find the angle of a vector

angle = atand(yv/xv);


if (xv < 0) && (yv > 0)
   angle = angle + 180;
elseif (xv < 0) && (yv < 0)
    angle = angle -180;
end

% if (angle > 0) && (yv < 0)
%    angle = -angle; 
% end

end

