function S=request(S, Preq)
  %request Function
  %random request to a destination node
  
  %number of nodes
  n=length(S);
  
  
  for ii=1:n
    if rand<Preq
        S(ii).DN=round(rand*n);	
        if S(ii).DN==0
            S(ii).DN=ii;
        end	
    end 
  end
  
    
end

