function S=findpaths(S, dmax)
%findopaths- FUCNTION
%function to find paths for transmission

%number of nodes
n=length(S);
mindis = 10000;

for i=1:n

    dest=min(n,S(i).DN);
    
    %find the distance to the destination node
    distance=sqrt( (S(i).xd-(S(S(i).DN).xd) )^2 + (S(i).yd-(S(S(i).DN).yd) )^2 );
    
    %find nearest nodes with respect to destination node
    for j=1:n
       distance1=sqrt( (S(j).xd-(S(S(i).DN).xd) )^2 + (S(j).yd-(S(S(i).DN).yd) )^2 );

       distance2=sqrt( (S(i).xd-S(j).xd)^2 + (S(i).yd-S(j).yd)^2 );

       if j~=i
           if j==1
                mindis=distance2;
                bN=j;  %broadcast node
           else
                if ((distance2)<mindis) && (distance2 < dmax) && ((distance1 + distance2) < 1.4* distance)
                    mindis = distance2;
                    bN=j;
                end
           end
       else
           mindis = distance;
           bN = dest;
       end
    end
    
    if (distance<dmax)
        S(i).RRQ = S(i).DN;
    else
        S(i).RRQ = bN;
    end

end

end