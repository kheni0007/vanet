function S=move(S,xm,ym,vmax)
 
  %number of nodes
  n=length(S);
  
  for ii=1:n
   
%     %calculate new velocities
%     S(ii).xv=(S(ii).xv+(rand*-1 +rand)*vmax*(1/sqrt(2)))/4;
%     S(ii).yv=(S(ii).yv+(rand*-1 +rand)*vmax*(1/sqrt(2)))/4;
% 	
% 	%limit the new velocities
% 	if S(ii).xv>vmax*(1/sqrt(2))
% 	  S(ii).xv=vmax*(1/sqrt(2));
% 	elseif S(ii).xv<-vmax*(1/sqrt(2))
% 	  S(ii).xv=-vmax*(1/sqrt(2));
% 	end
% 	
%     if S(ii).yv>vmax*(1/sqrt(2))
% 	  S(ii).yv=vmax*(1/sqrt(2));
% 	elseif S(ii).yv<-vmax*(1/sqrt(2))
% 	  S(ii).yv=-vmax*(1/sqrt(2));
% 	end
	
	%find the new positions
	S(ii).xd=S(ii).xd+S(ii).xv;
	S(ii).yd=S(ii).yd+S(ii).yv;
	
	%limit the positions within the geographical area
	S=bouncelimit(S,xm,ym);
	
  end  



end