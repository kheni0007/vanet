function S=bouncelimit(S,xm,ym)
    %bouncelimit- geographical constraint function
	
	%number of nodes
    n=length(S);	
	
	for ii=1:n
	  if S(ii).xd>xm
	    S(ii).xd=S(ii).xd+(xm-S(ii).xd);
        S(ii).xv = -S(ii).xv;
	  elseif S(ii).xd<0
	    S(ii).xd=-S(ii).xd;
        S(ii).xv = -S(ii).xv;
	  end
	  if S(ii).yd>ym
	    S(ii).yd=S(ii).yd+(ym-S(ii).yd);
        S(ii).yv = -S(ii).yv;
	  elseif S(ii).yd<0
	    S(ii).yd=-S(ii).yd;
        S(ii).yv = -S(ii).yv;
	  end
	
	end
	
	
end